﻿using System.IO;
using System.Linq;

namespace HexagonalGridRoute.Console {

    internal class HexagonalGridRouteConsole {

        private static void Main(string[] args) {
            // Read the input file as one string.
            string text = File.ReadAllText(@"input.txt");
            var directionList = text.Split(",").ToList();
            System.Console.WriteLine("Number of steps: {0}", Lib.HexagonalGridRoute.Trace(directionList).Distance);

            System.Console.ReadLine();
        }
    }
}