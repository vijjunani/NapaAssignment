using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;

namespace HexagonalGridRoute.Tests {

    [TestClass]
    public class HexagonalGridRouteTest {

        [TestMethod]
        public void HexagonalInvalidDirectionTest() {
            var list = new List<string>() { "e" };
            Assert.ThrowsException<ArgumentException>(() => Lib.HexagonalGridRoute.Trace(list).Distance);
        }

        [TestMethod]
        public void HexagonalTest1() {
            var list = new List<string>() { "ne", "ne", "ne" };
            var hexagon = Lib.HexagonalGridRoute.Trace(list);
            var distance = hexagon.Distance;
            Assert.AreEqual(3, distance);
        }

        [TestMethod]
        public void HexagonalTest2() {
            var list = new List<string>() { "ne", "ne", "sw", "sw" };
            var hexagon = Lib.HexagonalGridRoute.Trace(list);
            var distance = hexagon.Distance;
            Assert.AreEqual(0, distance);
        }

        [TestMethod]
        public void HexagonalTest3() {
            var list = new List<string>() { "ne", "ne", "s", "s" };
            var hexagon = Lib.HexagonalGridRoute.Trace(list);
            var distance = hexagon.Distance;
            Assert.AreEqual(2, distance);
        }

        [TestMethod]
        public void HexagonalTest4() {
            var list = new List<string>() { "se", "sw", "se", "sw", "sw" };
            var hexagon = Lib.HexagonalGridRoute.Trace(list);
            var distance = hexagon.Distance;
            Assert.AreEqual(3, distance);
        }
    }
}