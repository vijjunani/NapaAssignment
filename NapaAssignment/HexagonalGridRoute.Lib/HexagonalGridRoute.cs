﻿using System;
using System.Collections.Generic;

namespace HexagonalGridRoute.Lib {

    public struct Position {

        public Position(int x, int y, int z) {
            X = x; Y = y; Z = z;
        }

        internal int X { get; private set; }
        internal int Y { get; private set; }
        internal int Z { get; private set; }
    }

    public static class HexagonalGridRoute {
        private static readonly Hexagon ORIGIN_HEXAGON = new Hexagon(new Position(0, 0, 0));

        // Trace route along the given direction list
        public static Hexagon Trace(IList<string> directionList) {
            var currentHexagon = ORIGIN_HEXAGON;
            foreach (var dir in directionList) {
                currentHexagon = currentHexagon.Move(dir);
            }
            return currentHexagon;
        }
    }

    public class Hexagon {

        public Hexagon(Position positon) {
            Position = positon;
        }

        // Distance of this hexagon from the starting position i.e origin
        public int Distance => (Math.Abs(Position.X) + Math.Abs(Position.Y) + Math.Abs(Position.Z)) / 2;

        public Position Position { get; set; }

        // Get a new position from current position to a direction
        // Hexagonal grid coordinate system taken from https://www.redblobgames.com/grids/hexagons/
        public Hexagon Move(string direction) {
            switch (direction) {
                case "n":
                    return new Hexagon(new Position(Position.X + 0, Position.Y + 1, Position.Z - 1));

                case "s":
                    return new Hexagon(new Position(Position.X + 0, Position.Y - 1, Position.Z + 1));

                case "nw":
                    return new Hexagon(new Position(Position.X - 1, Position.Y + 1, Position.Z + 0));

                case "ne":
                    return new Hexagon(new Position(Position.X + 1, Position.Y + 0, Position.Z - 1));

                case "sw":
                    return new Hexagon(new Position(Position.X - 1, Position.Y + 0, Position.Z + 1));

                case "se":
                    return new Hexagon(new Position(Position.X + 1, Position.Y - 1, Position.Z + 0));

                default:
                    throw new ArgumentException(String.Format("{0} is not a valid direction", direction));
            }
        }
    }
}